$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/
		//Martinez Nava Neil Alejandro
		
		$.ajax({
			url: "http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra,
			success: function(respuesta) {
				
				setTimeout(function () {
					$('#loader').remove(); //Se elimina la imagen de "cargando" (los engranes)
	
					//Para cada elemento en la lista de resultados (para cada pelicula)
					$.each(respuesta.results, function(index, elemento) {
						miLista.append('<li>'+elemento.original_title+'</li>')
						//La función crearMovieCard regresa el HTML con la estructura de la pelicula
						//cardHTML = crearMovieCard(elemento); 
						//peliculas.append(cardHTML);
					});
	
				}, 3000); //Tarda 3 segundos en ejecutar la función de callback
						  //Sino no se vería la imagen de los engranes, da al usuario la sensación de que se está obteniendo algo.
	
			},
			error: function() {
				console.log("No se ha podido obtener la información");
				$('#loader').remove();
				$('#miLista').html('No se ha podido obtener la información');
			},
			beforeSend: function() { 
				//ANTES de hacer la petición se muestra la imagen de cargando.
				console.log('CARGANDO');
				$('#miLista').html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
			},
		});

	});

});


